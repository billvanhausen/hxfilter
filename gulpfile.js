// Include gulp
var gulp   = require('gulp'),
    eslint = require('gulp-eslint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

var projectName = "hxFilter";

var jsfiles = [
'src/*.js'
];    

// Lint Task    
gulp.task('lint', function () {
    var lintOpts = {
        useEslintrc: true
    };
    
    return gulp.src(jsfiles, {base: 'Assets/src/'})
        .pipe(eslint(lintOpts))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// Minify JS
gulp.task('scripts', function() {
    var uglyOpts = {
        mangle: false
    };
    
    return gulp.src(jsfiles, {base: 'src/'})
        .pipe(gulp.dest('js/'))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(uglify(uglyOpts))
        .pipe(gulp.dest('js/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(jsfiles, ['lint', 'scripts']);
});

// Default Task
gulp.task('default', ['lint', 'scripts', 'watch']);