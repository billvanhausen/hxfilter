/*  
hxFilter.js (v2.1.5)

This jQuery utility was developed by Bill Hicks

Dependencies:
    jQuery      (v1.11.3+)

    The Date Range filter requires daterangepicker.js from:
    http://www.daterangepicker.com/
    [In turn daterangepicker.js requires: Bootstrap, jQuery, and Moment.js]
 
MIT License:
    Copyright (c) 2015 Bill Hicks billvanhausen@yahoo.com
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of 
    this software and associated documentation files (the "Software"), to deal in 
    the Software without restriction, including without limitation the rights to 
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all 
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  
*/

/*global jQuery*/

(function ($) {
    "use strict";
    if(typeof Array.hx_contains === "undefined" && typeof Array.hx_unique === "undefined"){
        
        Array.prototype.hx_contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };
        
        Array.prototype.hx_unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.hx_contains(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        };
    }
    
    var _filter = {};
    
    var _private = { 
        selector: "",
        defaults: {
            value: "",
            style: ""
        },
        data: [],
        filters: {
            checkboxes: [],
            dropdowns: [],
            minmax: [],
            dateranges: []
        },
        setData: function(selector, data, callback){
            this.data = data;
            this.callback = callback;
            this.selector = selector;
        },
        setActions: function(){
            // Need to clear the defaults or everything following will inherit it
            this.defaults = {
                value: "",
                style: ""
            };
            
            var $this = this;
            
            $($this.selector + " form.hxFilter").unbind("change");
            
            $($this.selector + " form.hxFilter").on("change", function(){
                $this.filter($this);
            });  
        },
        checkbox: function( options, element ){
            var opts = $.extend(this.defaults, options);
            
            if(this.data.length === 0){ 
                console.error("No data found!");
                return;
            }
            
            if(opts.value === ""){ 
                console.error("No value found!");
                return;
            }
            var list = [];
            
            this.data.forEach(function(item, index, array){
                
                list.push(item[opts.value]);
                
            });
            
            list = list.hx_unique();
            
            if(typeof list[0] === "undefined"){
                console.error("Value '" + opts.value + "' not in list!");
                return;
            }
            
            // Build filter in the designated selector
            var frm = $("<form>", { class: "hxFilter", "data-val": opts.value }).appendTo(element.selector +" "+ opts.selector);
            
            if(opts.all){
                var allLabel = $("<label>", {
                    class: opts.style
                }).appendTo(frm);
            
                $("<input>",{
                    type: "checkbox",
                    value: "all",                        
                    checked: false
                }).appendTo(allLabel);  
                
                allLabel.append(" All"); 
            }
                
            list.forEach(function(val, index, array){
                var label = $("<label>", {
                    class: opts.style
                }).appendTo(frm);
                
                $("<input>",{
                    type: "checkbox",
                    value: val,
                    checked: true
                }).appendTo(label);  
                
                label.append(" " + val);                
            });
            
            
            this.filters.checkboxes.push(frm[0]);
            if(typeof opts.init === "function"){
                opts.init(frm[0]);    
            }
            this.setActions();
                
        },
        dropdown: function( options, element ){
            var opts = $.extend(this.defaults, options);
            
            if(this.data.length === 0){ 
                console.error("No data found!");
                return;
            }
            
            if(opts.value === ""){ 
                console.error("No value found!");
                return;
            }
            
            var list = [];
            this.data.forEach(function(item, index, array){
                list.push(item[opts.value]);
            });
            
            list = list.hx_unique();
            
            if(typeof list[0] === "undefined"){
                console.error("Value '" + opts.value + "' not in list!");
                return;
            }
            
            list.sort(); 
            
            //Build filter in the designated selector
            var frm = $("<form>", { class: "hxFilter", "data-val": opts.value }).appendTo(element.selector +" "+ opts.selector),
                select = $("<select>", {
                    class: opts.style
                }).appendTo(frm); 
            
            if(opts.showAll){
                $("<option>",{
                    text:  "All",
                    value: "All"
                }).appendTo(select);
            }
            console.dir(list);    
            list.forEach(function(val, index, array){                
                $("<option>",{
                    text: val,
                    value: val
                }).appendTo(select);                    
            });
            
            this.filters.dropdowns.push(frm[0]);
            if(typeof opts.init === "function"){
                opts.init(frm[0]);   
            }
            this.setActions();
        },
        minmax: function( options, element ){
            var opts = $.extend(this.defaults, options);
            
            if(this.data.length === 0){ 
                console.error("No data found!");
                return;
            }
            
            if(opts.value === ""){ 
                console.error("No value found!");
                return;
            }
            var list = [];
            this.data.forEach(function(item, index, array){
                list.push(item[opts.value]);
            });
            
            list = list.hx_unique();
            
            if(typeof list[0] === "undefined"){
                console.error("Value '" + opts.value + "' not in list!");
                return;
            }
            
            // Sort data 
            list.sort(function(a, b){
                a = parseInt(a);
                b = parseInt(b);
                return a-b;
            });
            
            // Build filter in the designated selector
            var frm = $("<form>", { class: "hxFilter", "data-val": opts.value }).appendTo(element.selector +" "+ opts.selector),
                min = $("<label>", { 
                    text: "Minimum"
                }).appendTo(frm),
                max = $("<label>", {
                    text: "Maximum"
                }).appendTo(frm);
                
            $("<input>", {
                type: "text",
                class: "hxFMin " + opts.style, 
                value: list[0]
            }).appendTo(min); 
            
            $("<input>", {
                type: "text",
                class: "hxFMax " + opts.style, 
                value: list.slice(-1)[0]
            }).appendTo(max); 
                
            this.filters.minmax.push(frm[0]);
            if(typeof opts.init === "function"){
                opts.init(frm[0]);   
            }
            
            this.setActions();
            
        },
        daterange: function( options, element ){
            /*
             * The daterange filter requires daterangepicker.js
             * (See notice above)
             */
            if(typeof $().daterangepicker != "function"){
                console.error("The Date Range filter requires daterangepicker.js");
                return;
            }
            
            var opts = $.extend(this.defaults, options);
            
            if(this.data.length === 0){ 
                console.error("No data found!");
                return;
            }
            
            if(opts.value === ""){ 
                console.error("No value found!");
                return;
            }
            
            
            var list = [];
            this.data.forEach(function(item, index, array){
                var d = new Date(item[opts.value]),
                    sortObject = {
                        string: item[opts.value],
                        posix: d.getTime(),
                        local: d.get
                    };
                list.push(sortObject);
            });
            list.sort( function( a, b ){ return a.posix - b.posix; } );
            // console.dir( list );
            
            var dateFrom = list[0],
                dateTo   = list.slice(-1)[0],
                initialRange = dateFrom.string + " - " + dateTo.string;
            
            //Build filter in the designated selector
            var frm = $("<form>", { class: "hxFilter", "data-val": opts.value }).appendTo(element.selector +" "+ opts.selector);

            $("<input>", {
                type: "text",
                value: initialRange,
                class: opts.style
            }).appendTo(frm); 
            
            $(frm[0][0]).daterangepicker({
                locale: {
                    format: "YYYY-MM-DD"
                },
                startDate: dateFrom.string,
                endDate: dateTo.string
            });         
            
            this.filters.dateranges.push(frm[0]);
            if(typeof opts.init === "function"){
                opts.init(frm[0]);   
            }
            this.setActions();
        },
        filter: function(){
            var app = this,
                fdata = this.data;
            
        // Exclusive filters
            // Dropdowns
            app.filters.dropdowns.forEach(function(input, index, array){
                // console.dir(arguments);
                var $key = $(input).data("val"),
                    $value = input[0].value;
                    
                if($value !== "All"){
                    fdata = $.grep(fdata, function(itm){
                        // console.log(itm[$key] + " " + $value + " [" + (itm[$key] === $value) + "]");
                        return (itm[$key] === $value);
                    });                    
                }
                
            });
            app.filters.dropdowns.forEach(function(input, index, array){
                // console.dir(arguments);
                var $key = $(input).data("val"),
                    $value = input[0].value;
                    
                if($value === "All"){
                    var select = $(input[0]);
                    select.html("");
                    $("<option>",{
                        text:  "All",
                        value: "All"
                    }).appendTo(select);
                    
                    console.dir(fdata);
                    var items = [];
                    fdata.forEach(function(val, index, array){
                        items.push(val[$key]);                  
                    });
                    
                    items = items.hx_unique();
                    items.sort();
                    
                    items.forEach(function(txt){
                        
                        $("<option>",{
                            text: txt,
                            value: txt
                        }).appendTo(select);  
                        
                    });
                    
                }
                
            });
            
            // Date Range
            var moment = moment || {}; // to keep linter happy
            app.filters.dateranges.forEach(function(input, index, array){
                var $key = $(input).data("val"),
                    $value = input[0].value,
                    dateFrom = moment($value.split(" - ")[0]),
                    dateTo   = moment($value.split(" - ")[1]);
                    
                fdata = $.grep(fdata, function(itm){
                    var dateTest = moment(itm[$key]);
                    return (dateTest.isBetween(dateFrom.subtract(1, "days"), dateTo.add(1, "days")));                    
                });                     
                    
            });  
            
            // Min Max
            app.filters.minmax.forEach(function(input, index, array){
                var $key = $(input).data("val"),
                    $min = parseInt(input[0].value),
                    $max = parseInt(input[1].value);
                    
                if(isNaN($min) || isNaN($max)){
                    console.error("Both values must be numbers.");
                    return;
                }
                
                if($min > $max){
                    console.error("The maximum value cannot be less than the minimum value");
                    input[0].value = $max;
                }
                    
                fdata = $.grep(fdata, function(itm){
                    var $age = parseInt(itm[$key]);
                    return ($min <= $age && $age <= $max);
                });                     
                    
            });    
            
            // Inclusive Filters
            app.filters.checkboxes.forEach(function(input, index, array){
                
                if(input[0].checked && input[0].value === "all"){
                    for(var set = 0; set < input.length; set++){
                        input[set].checked = true;
                    }
                }    
                var idata = [],
                    $key = $(input).data("val");
                    
                for(var box = 0; box < input.length; box++){
                    var $value = input[box].value;
                    if(input[box].checked){
                        var f = $.grep(fdata, function(itm){
                            return (itm[$key] === $value);
                        });
                        idata = idata.concat(f);
                    }
                }
                idata = idata.hx_unique();
                fdata = idata;
            });
            
            for(var d = 0; d < this.filters["dropdowns"]; d++){
                
                console.dir(this.filters["dropdowns"][d]);
            }
            
            app.callback(fdata);
            
        }        
    };
    
    
    $.fn.hxData = function( data, callback ){
        var $this = this;
         
        _filter[$this.selector] = $.extend(true, {}, _private); 
        
        _filter[$this.selector].setData( $this.selector, data, callback );
        
    };
    
    $.fn.hxCheckbox = function( options ){
        if(typeof _filter[this.selector] != "undefined"){
            _filter[this.selector].checkbox( options, this );
        }
    };
    
    $.fn.hxDropdown = function( options ){
        if(typeof _filter[this.selector] != "undefined"){
            _filter[this.selector].dropdown( options, this );
        }
    };
    
    $.fn.hxMinMax = function( options ){
        if(typeof _filter[this.selector] != "undefined"){
            _filter[this.selector].minmax( options, this );
        }
    };
    
    $.fn.hxDaterange = function( options ){
        if(typeof _filter[this.selector] != "undefined"){
            _filter[this.selector].daterange( options, this );
        }
    };
    
    $.fn.hxRun = function(){
        _filter[this.selector].filter();
    };
    
}(jQuery));